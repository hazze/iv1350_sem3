package testModel;

import org.junit.Before;
import org.junit.After;
import org.junit.Test;
import sem3.model.InspectionResult;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

public class TestInspectionResult
{
    private InspectionResult testResult;
    @Before
    public void setUp()
    {
        testResult = new InspectionResult("No problems found", true);
    }

    @After
    public void tearDown()
    {
        testResult = null;
    }

    @Test
    public void testResultConstructorInspectionComments()
    {
        String expResult = "No problems found";
        String result = testResult.getInspectComment();
        assertEquals("Inspection comment string nog equal", expResult, result);
    }

    @Test
    public void testResultConstructorInspectionResult()
    {
        boolean expResult = true;
        boolean result = testResult.isInspectResult();
        assertEquals("Inspection comment string nog equal", expResult, result);
    }

    @Test
    public void testResultSetInspectionComments()
    {
        String expResult = "Don't drive this car";
        testResult.setInspectComment("Don't drive this car");
        String result = testResult.getInspectComment();
        assertEquals("Inspection comment string not equal", expResult, result);
    }

    @Test
    public void testResultSetInspectionResult()
    {
        boolean expResult = false;
        testResult.setInspectResult(false);
        boolean result = testResult.isInspectResult();
        assertEquals("Inspection steps string not equal", expResult, result);
    }

    @Test
    public void testResultToString()
    {
        final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        System.out.print(testResult.toString());
        assertEquals("Inspection result: true, Inspection comments: No problems found", outContent.toString());

        System.setOut(null);
    }
}
