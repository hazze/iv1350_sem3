package testModel;

import org.junit.Before;
import org.junit.After;
import org.junit.Test;
import sem3.model.CreditCard;

import static org.junit.Assert.*;

public class TestCreditCard
{
    private CreditCard testCard;

    @Before
    public void setUp()
    {
        testCard = new CreditCard(1234, 123, "1234 1234 1234 1234", "John Doe", "19-04");
    }

    @After
    public void tearDown()
    {
        testCard = null;
    }

    @Test
    public void testCorrectCreditCardPin()
    {
        int expResult = 1234;
        int result = testCard.getPin();
        assertEquals("Pins not equal", expResult, result);
    }

    @Test
    public void testCorrectCreditCardCvc()
    {
        int expResult = 123;
        int result = testCard.getCvc();
        assertEquals("CVC not equal", expResult, result);
    }

    @Test
    public void testCorrectCreditCardCardNumber()
    {
        String expResult = "1234 1234 1234 1234";
        String result = testCard.getNumber();
        assertEquals("Card Number not equal", expResult, result);
    }

    @Test
    public void testCorrectCreditCardHolder()
    {
        String expResult = "John Doe";
        String result = testCard.getHolder();
        assertEquals("Holder not equal", expResult, result);
    }

    @Test
    public void testCorrectCreditCardDate()
    {
        String expResult = "19-04";
        String result = testCard.getDate();
        assertEquals("Dates not equal", expResult, result);
    }



    @Test
    public void testWrongCreditCardPin()
    {
        int expResult = 1235;
        int result = testCard.getPin();
        assertNotEquals("Pins is equal", expResult, result);
    }

    @Test
    public void testWrongCreditCardCvc()
    {
        int expResult = 124;
        int result = testCard.getCvc();
        assertNotEquals("CVC is equal", expResult, result);
    }

    @Test
    public void testWrongCreditCardCardNumber()
    {
        String expResult = "1234 1234 1234 1235";
        String result = testCard.getNumber();
        assertNotEquals("Card Number is equal", expResult, result);
    }

    @Test
    public void testWrongCreditCardHolder()
    {
        String expResult = "John Done";
        String result = testCard.getHolder();
        assertNotEquals("Holder is equal", expResult, result);
    }

    @Test
    public void testWrongCreditCardDate()
    {
        String expResult = "19-05";
        String result = testCard.getDate();
        assertNotEquals("Dates is equal", expResult, result);
    }
}
