package testModel;

import org.junit.Before;
import org.junit.After;
import org.junit.Test;
import sem3.model.InspectionChecklist;

import static org.junit.Assert.*;

public class TestInspectionChecklist
{
    private InspectionChecklist testChecklist;
    @Before
    public void setUp()
    {
        testChecklist = new InspectionChecklist("Light test", "Step 1. Turn on lights, Step 2. Look at lights.");
    }

    @After
    public void tearDown()
    {
        testChecklist = null;
    }

    @Test
    public void testChecklistConstructorInspectionId()
    {
        String expResult = "Light test";
        String result = testChecklist.getInspectionId();
        assertEquals("Inspection ID string not equal", expResult, result);
    }

    @Test
    public void testChecklistConstructorInspectionSteps()
    {
        String expResult = "Step 1. Turn on lights, Step 2. Look at lights.";
        String result = testChecklist.getInspectionSteps();
        assertEquals("Inspection steps string not equal", expResult, result);
    }

    @Test
    public void testChecklistSetInspectionId()
    {
        String expResult = "Brake test";
        testChecklist.setInspectionId("Brake test");
        String result = testChecklist.getInspectionId();
        assertEquals("Inspection ID string not equal", expResult, result);
    }

    @Test
    public void testChecklistSetInspectionSteps()
    {
        String expResult = "Don't do stupid stuff";
        testChecklist.setInspectionSteps("Don't do stupid stuff");
        String result = testChecklist.getInspectionSteps();
        assertEquals("Inspection steps string not equal", expResult, result);
    }
}
