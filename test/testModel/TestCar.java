package testModel;

import org.junit.Before;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;

import sem3.model.Car;

public class TestCar
{
    private Car testCar;
    @Before
    public void setUp()
    {
        testCar = new Car("ABC123");
    }

    @After
    public void tearDown()
    {
        testCar = null;
    }

    @Test
    public void testNewCorrectCar()
    {
        String expResult = "ABC123";
        String result = testCar.getRegNumber();
        assertEquals("Wrong registration number found", expResult, result);
    }

    @Test
    public void testNewWrongCar()
    {
        String expResult = "ABC124";
        String result = testCar.getRegNumber();
        assertNotEquals("Wrong registration number found", expResult, result);
    }
}
