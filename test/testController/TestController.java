package testController;

import org.junit.Before;
import org.junit.After;
import org.junit.Test;
import sem3.controller.Controller;
import sem3.data.Database;
import sem3.integration.Display;
import sem3.integration.Inspection;
import sem3.model.InspectionChecklist;
import sem3.model.InspectionResult;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.Array;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class TestController
{
    private Controller testController;
    private Database db;

    @Before
    public void setUp() throws IOException {
        testController = new Controller();
        db = new Database();
    }

    @After
    public void tearDown()
    {
        testController = null;
        db = null;
    }

    @Test
    public void testEnterRegNumber() throws Exception {
        double expResult = 342.9;
        double result = testController.enterRegNumber("ABC123");
        assertEquals("Price is not equal", expResult, result, 0.001d);
    }

    @Test
    public void testNextInspection()
    {
        Inspection expResult = new Inspection("ABC123", 40, 1);

        ArrayList<Inspection> testInspections = new ArrayList<>();
        Inspection testInspection1 = new Inspection("ABC123", 40, 1);
        testInspection1.getChecklist().setInspectionId("Dummy Test Inspection 0");
        Inspection testInspection2 = new Inspection("ABC123", 40, 1);
        testInspection2.getChecklist().setInspectionId("Dummy Test Inspection 1");
        Inspection testInspection3 = new Inspection("ABC123", 40, 1);
        testInspection3.getChecklist().setInspectionId("Dummy Test Inspection 2");
        testInspections.add(testInspection1);
        testInspections.add(testInspection2);
        testInspections.add(testInspection3);

        Inspection result = new Inspection("ABC123", 40, 1);
        for (int i = 0; i < testInspections.size(); i++)
        {
            expResult.getChecklist().setInspectionId("Dummy Test Inspection " + i);
            result.getChecklist().setInspectionId(testController.nextInspection(i, testInspections).getInspectionId());
            assertEquals("Not the same inspection", expResult.getChecklist().getInspectionId(), result.getChecklist().getInspectionId());
        }
    }
}
