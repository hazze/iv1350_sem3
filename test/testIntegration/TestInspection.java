package testIntegration;

import org.junit.Before;
import org.junit.After;
import org.junit.Test;

import static org.junit.Assert.*;
import sem3.integration.Inspection;

import java.util.ArrayList;

public class TestInspection
{
    private Inspection testInspection;
    @Before
    public void setUp()
    {
        testInspection = new Inspection("ABC123", 40, 0);
    }

    @After
    public void tearDown()
    {
        testInspection = null;
    }
/*
    @Test
    public void testInspectionCar()
    {
        Inspection expResult = new Inspection("ABC123", 40);
        Inspection result = testInspection;
    }
*/

    @Test
    public void testCalculateCost()
    {
        Inspection insp1 = new Inspection("ABC123", 40.2, 0);
        Inspection insp2 = new Inspection("ABC123", 20.2, 1);
        Inspection insp3 = new Inspection("ABC123", 30.5, 2);
        Inspection insp4 = new Inspection("ABC123", 70.3, 3);

        ArrayList<Inspection> testInspections = new ArrayList<>();
        testInspections.add(insp1);
        testInspections.add(insp2);
        testInspections.add(insp3);
        testInspections.add(insp4);

        double expResult = 161.2;
        double result = Inspection.calculateCost(testInspections);
        assertEquals("Price is not equal", expResult, result, 0.001d);
    }
}
