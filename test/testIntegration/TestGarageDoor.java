package testIntegration;

import org.junit.Before;
import org.junit.After;
import org.junit.Test;
import sem3.integration.GarageDoor;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

public class TestGarageDoor
{
    private GarageDoor testGarageDoor;
    @Before
    public void setUp()
    {

    }

    @After
    public void tearDown()
    {

    }

    @Test
    public void testOperateDoor()
    {
        final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        GarageDoor testGarageDoor = new GarageDoor(false);
        System.setOut(new PrintStream(outContent));

        testGarageDoor.operateDoor(false);
        assertEquals("GarageDoor open: false\n", outContent.toString());

        System.setOut(null);
    }
}
