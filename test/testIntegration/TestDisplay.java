package testIntegration;

import org.junit.Before;
import org.junit.After;
import org.junit.Test;
import sem3.integration.Display;

import static org.junit.Assert.*;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class TestDisplay
{
    @Before
    public void setUp()
    {

    }

    @After
    public void tearDown()
    {

    }

    @Test
    public void testDisplayNumber()
    {
        final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        Display testDisplay = new Display(5);
        System.setOut(new PrintStream(outContent));

        testDisplay.incrementNumber();
        System.out.print(testDisplay.toString());
        assertEquals("Now serving: 6", outContent.toString());

        System.setOut(null);
    }
}
