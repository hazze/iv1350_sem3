package testIntegration;

import org.junit.Before;
import org.junit.After;
import org.junit.Test;
import sem3.integration.DatabaseManager;
import sem3.integration.Inspection;
import sem3.integration.noDatabaseException;
import sem3.integration.noInspectionsFoundException;

import static org.junit.Assert.*;
import java.util.ArrayList;

public class TestDatabaseManager
{
    private DatabaseManager testDbManager;
    @Before
    public void setUp()
    {
        testDbManager = new DatabaseManager();
    }

    @After
    public void tearDown()
    {
        testDbManager = null;
    }

    @Test
    public void testFindInspectionByRegNumber() throws noInspectionsFoundException, noDatabaseException
    {
        int expResult = 5;
        int result = testDbManager.findInspectionByRegNumber("ABC123").size();
        assertEquals("Not the same number of hits", expResult, result);
    }

    @Test
    public void testStoreInspections()
    {
        Inspection expResult = new Inspection("ABC123", 40.0, 0);
        expResult.getResult().setInspectComment("Dummy comment");
        expResult.getResult().setInspectResult(true);
        Inspection result = new Inspection("ABC123", 40.0, 0);
        testDbManager.storeResult("Dummy comment", true, 0);
        result.getResult().setInspectResult(testDbManager.db.dummyInspections.get(0).getResult().isInspectResult());
        result.getResult().setInspectComment(testDbManager.db.dummyInspections.get(0).getResult().getInspectComment());

        // Convert saved input to strings and compare because I never managed to compare two objects to each other
        String expResultString = "" + expResult.getResult().getInspectComment() + expResult.getResult().toStringBoolean();
        String resultString = "" + result.getResult().getInspectComment() + result.getResult().toStringBoolean();
        assertEquals(expResultString, resultString);
    }
}
