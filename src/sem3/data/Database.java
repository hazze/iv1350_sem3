package sem3.data;

import sem3.integration.Inspection;

import java.util.ArrayList;

/*
 *  Placeholder for the database
 */
public class Database
{
    // Dummy database declaration by using a list.
    public ArrayList<Inspection> dummyInspections = new ArrayList<>();

    /*
     *  Creates a new instance of Database.
     */
    public Database()
    {
        Inspection insp1 = new Inspection("ABC123", 40.0, 0);
        Inspection insp2 = new Inspection("ABC124", 20.2, 1);
        Inspection insp3 = new Inspection("ABC125", 30.5, 2);
        Inspection insp4 = new Inspection("ABC123", 70.3, 3);
        Inspection insp5 = new Inspection("ABC124", 70.3, 4);
        Inspection insp6 = new Inspection("ABC124", 421.0, 5);
        Inspection insp7 = new Inspection("ABC123", 70.3, 6);
        Inspection insp8 = new Inspection("ABC123", 70.3, 7);
        Inspection insp9 = new Inspection("ABC125", 7.0, 8);
        Inspection insp10 = new Inspection("ABC125", 32.3, 9);
        Inspection insp11 = new Inspection("ABC123", 92.0, 10);

        insp1.getChecklist().setInspectionId("Dummy Inspection ID");
        insp1.getChecklist().setInspectionSteps("Dummy Inspection Steps");
        insp2.getChecklist().setInspectionId("Dummy Inspection ID");
        insp2.getChecklist().setInspectionSteps("Dummy Inspection Steps");
        insp3.getChecklist().setInspectionId("Dummy Inspection ID");
        insp3.getChecklist().setInspectionSteps("Dummy Inspection Steps");
        insp4.getChecklist().setInspectionId("Dummy Inspection ID");
        insp4.getChecklist().setInspectionSteps("Dummy Inspection Steps");
        insp5.getChecklist().setInspectionId("Dummy Inspection ID");
        insp5.getChecklist().setInspectionSteps("Dummy Inspection Steps");
        insp6.getChecklist().setInspectionId("Dummy Inspection ID");
        insp6.getChecklist().setInspectionSteps("Dummy Inspection Steps");
        insp7.getChecklist().setInspectionId("Dummy Inspection ID");
        insp7.getChecklist().setInspectionSteps("Dummy Inspection Steps");
        insp8.getChecklist().setInspectionId("Dummy Inspection ID");
        insp8.getChecklist().setInspectionSteps("Dummy Inspection Steps");
        insp9.getChecklist().setInspectionId("Dummy Inspection ID");
        insp9.getChecklist().setInspectionSteps("Dummy Inspection Steps");
        insp10.getChecklist().setInspectionId("Dummy Inspection ID");
        insp10.getChecklist().setInspectionSteps("Dummy Inspection Steps");
        insp11.getChecklist().setInspectionId("Dummy Inspection ID");
        insp11.getChecklist().setInspectionSteps("Dummy Inspection Steps");

        dummyInspections.add(insp1);
        dummyInspections.add(insp2);
        dummyInspections.add(insp3);
        dummyInspections.add(insp4);
        dummyInspections.add(insp5);
        dummyInspections.add(insp6);
        dummyInspections.add(insp7);
        dummyInspections.add(insp8);
        dummyInspections.add(insp9);
        dummyInspections.add(insp10);
        dummyInspections.add(insp11);
    }
}
