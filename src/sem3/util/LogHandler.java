package sem3.util;
import java.io.*;
import java.time.*;
import java.time.format.DateTimeFormatter;


/**
 * Class to handle the logging
 */
public class LogHandler
{
    private static final String LOG_FILE_NAME = "inspectcar-log.txt";

    private PrintWriter logFile;

    /**
     * Creates an new object of LogHandler.
     * @throws IOException Exception thrown if file is not created.
     */
    public LogHandler() throws IOException
    {
        logFile = new PrintWriter(new FileWriter(LOG_FILE_NAME, true));
    }

    /**
     * Creates the default string with time and message. Prints everything to the newly created file.
     * @param exception Takes the currently thrown exception to log.
     */
    public void logException(Exception exception)
    {
        StringBuilder logMsgBuilder = new StringBuilder();
        logMsgBuilder.append(fetchTime());
        logMsgBuilder.append(" - Exception was thrown: ");
        logMsgBuilder.append(exception.getMessage());
        logFile.println(logMsgBuilder + "\n");
        exception.printStackTrace(logFile);
        logFile.println("\n\n\n");
        logFile.close();
    }

    /**
     * Method to fetch current time and date.
     * @return Returns current time and date in ISO-format.
     */
    private String fetchTime()
    {
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
        return now.format(formatter);
    }
}
