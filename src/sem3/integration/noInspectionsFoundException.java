package sem3.integration;

/**
 * Class for the custom exception when no inspections were found.
 */
public class noInspectionsFoundException extends Exception
{
    /**
     * Create a new instance of the new exception
     * @param msg Receives the error message.
     */
    public noInspectionsFoundException (String msg)
    {
        super(msg);
    }
}
