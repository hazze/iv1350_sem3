package sem3.integration;

import sem3.data.Database;
import sem3.model.InspectionObserver;

import java.util.ArrayList;
import java.util.List;

/*
 *  Contains the database manger declaration and all the methods needed for it to function.
 */
public class DatabaseManager
{
    // Init dummy DB
    public Database db = new Database();

    private ArrayList<InspectionObserver> inspectionObservers = new ArrayList<>();

    /*
     *  Creates an instance of the database manager used to control the database.
     */
    public DatabaseManager()
    {

    }

    /*
     *  Method to find inspections tied to a specific registration number.
     *  Saves all found inspections to a list if the registration number matches.
     *  Returns the list to the controller.
     *
     *  @param regNumber The registration number to search for.
     *  @return A list of Inspections that was found for the registration number.
     *  @exception noDatabaseException Thrown when the database is unreachable.
     *  @exception noInspectionsFoundException Thrown when no inspections are found.
     */
    public ArrayList<Inspection> findInspectionByRegNumber(String regNumber) throws noDatabaseException, noInspectionsFoundException
    {
        ArrayList<Inspection> foundInspections = new ArrayList<>();
        if (db == null)
        {
            throw new noDatabaseException("Error. Unable to connect to database.\nCheck connection status.");
        }

        for (int i = 0; i < db.dummyInspections.size(); i++)
        {
            if (db.dummyInspections.get(i).getCar().getRegNumber().equals(regNumber))
            {
                foundInspections.add(db.dummyInspections.get(i));
            }
        }
        if (foundInspections.size() == 0)
        {
            throw new noInspectionsFoundException("No inspections found for number: " + regNumber + ".\nCheck Db status.");
        }

        return foundInspections;
    }

    /*
     *  Method to store new inspections results directly to the db.
     *  Takes the inspection comment, result and a list index as argument¨
     *
     *  @param inspectComment   Takes the inspection comment and saves it to the specified database entry.
     *  @param inspectResult    Takes the inspection result and saves it to the specified database entry.
     *  @param i    Index for the "original" database-slot so the results are saved on the correct place.
     */
    public void storeResult(String inspectComment, boolean inspectResult, int i)
    {
        db.dummyInspections.get(i).getResult().setInspectResult(inspectResult);
        db.dummyInspections.get(i).getResult().setInspectComment(inspectComment);

        notifyObservers(db.dummyInspections.get(i));
    }

    /**
     * Method to notify observers of change.
     * @param inspection    Receives an inspection that has changed to send to the observers
     */
    private void notifyObservers(Inspection inspection)
    {
        for (InspectionObserver obs : inspectionObservers)
        {
            obs.newInspection(inspection);
        }
    }

    /**
     * Method to add single observer
     * @param obs   Receives an observer to add to the list of existing.
     */
    public void addInspectionObserver(InspectionObserver obs)
    {
        inspectionObservers.add(obs);
    }

    /**
     * Method to add all observers.
     * @param observers     Receives a list of observers and adds all of them to the list of existing ones.
     */
    public void addInspectionObservers(ArrayList<InspectionObserver> observers)
    {
        inspectionObservers.addAll(observers);
    }
}
