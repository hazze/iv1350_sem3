package sem3.integration;

import sem3.model.*;

import java.util.ArrayList;

/*
 *  Contains the inspection declaration and with its get methods
 *  Also contains the method to calculate the cost of inspections performed.
 */
public class Inspection
{
    private Car car;
    private InspectionChecklist checklist;
    private InspectionResult result;
    private double price;
    private int dbFieldIndex;

    /**
     * Creates a new instance of inspection.
     * Takes a registration number and a price on creation.
     * Also contains new instances of the objects that is needed in an inspection.
     * @param regNumber Reference to the regNumber
     * @param price Reference to the price
     * @param dbFieldIndex Reference to the database field index, later used to return
     *                     the result to the original inspection in the database.
     */
    public Inspection(String regNumber, double price, int dbFieldIndex)
    {
        this.car = new Car(regNumber);
        this.checklist = new InspectionChecklist(null, null);
        this.result = new InspectionResult(null, false);
        this.price = price;
        this.dbFieldIndex = dbFieldIndex;
    }

    /*
     *  Method to fetch the info from the car object.
     *  @return Returns the car.
     */
    Car getCar()
    {
        return car;
    }

    /*
     *  Method to fetch the info from the checklist object.
     *  @return Returns the checklist.
     */
    public InspectionChecklist getChecklist()
    {
        return checklist;
    }

    /*
     *  Method to fetch the info from the result object.
     *  @return Returns the result.
     */
    public InspectionResult getResult()
    {
        return result;
    }

    /*
     *  Method to fetch the price info.
     *  @return Returns the price.
     */
    private double getPrice()
    {
        return price;
    }

    /*
     *  Method to fetch database field index.
     *  @return Returns the dbFieldIndex.
     */
    public int getDbFieldIndex()
    {
        return dbFieldIndex;
    }

    /*
     *  Method to calculate cost of all the found inspections.
     *  Receives the list of found inspections as an argument.
     *
     *  @param inspections Receives a list of inspections to calculate price.
     */
    public static double calculateCost(ArrayList<Inspection> inspections)
    {
        double cost = 0;
        for (Inspection inspection : inspections)
            cost += inspection.getPrice();

        return cost;
    }
}
