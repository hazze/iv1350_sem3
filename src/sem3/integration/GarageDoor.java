package sem3.integration;

/*
 *  Contains the garage door declaration and the method to open and close the door.
 */
public class GarageDoor
{
    // Default setting for the door is closed.
    private boolean doorStatus = false;

    /*
     *  Creates a new instance of the garage door.
     *  Receives the door status on creation.
     *
     *  @param doorStatus Reference to the door status, true for open.
     */
    public GarageDoor(boolean doorStatus)
    {
        this.doorStatus = doorStatus;
    }

    /*
     *  Method to operate the garage door
     *  Takes a boolean as argument. true = open, false = closed.
     *
     *  @param doorStatus Receive the status to put the door in.
     */
    public void operateDoor (boolean doorStatus)
    {
        this.doorStatus = doorStatus;
        System.out.println(this.toString());
    }

    /*
     *  Method to print garage door status due to the lack of a actual garage door.
     */
    @Override
    public String toString()
    {
        return "GarageDoor open: " + doorStatus;
    }
}
