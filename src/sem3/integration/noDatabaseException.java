package sem3.integration;

/**
 * Class for the custom exception when the database don't exist.
 */
public class noDatabaseException extends Exception
{
    /**
     * Create a new instance of the new exception
     * @param msg Receives the error message.
     */
    public noDatabaseException(String msg)
    {
        super(msg);
    }
}
