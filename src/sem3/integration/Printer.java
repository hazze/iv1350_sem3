package sem3.integration;

import java.util.ArrayList;

/*
 *  Printer class.
 *  Contains the printer object and the methods to print result and receipt.
 */
public class Printer
{
    /*
     *  Creates a new instance of the printer
     */
    public Printer()
    {

    }

    /*
     *  Method to print receipt.
     *  Receives the price as a argument to be able to print it on the receipt.
     *
     *  @param cost Receives the cost to print.
     */
    public void printReceipt(double cost)
    {
        System.out.println("Receipt: " + cost + " Monnies");
    }

    /*
     *  Method to print the result of the inspections.
     *  Receives the inspections list containing all the info of each inspection.
     *
     *  @param inspections Receives a list of inspections to print result from.
     */
    public void printResult(ArrayList<Inspection> inspections)
    {
        System.out.println("Result of inspections for car: " + inspections.get(0).getCar().getRegNumber());
        for (Inspection inspection : inspections)
            System.out.println(inspection.getResult().toString());
    }
}
