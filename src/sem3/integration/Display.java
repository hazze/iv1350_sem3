package sem3.integration;

/*
 *  Contains the display declaration and all the methods needed for it to function
 */
public class Display
{
    // Public int to hold the currently displayed number.
    private int currentNumber;

    /*
     *  Creates a new instance of the display.
     *  It hold the current number that is set with the starting number from the argument.
     *  @param startNumber Initial number for the display to start with on creation.
     */
    public Display(int startNumber)
    {
        this.currentNumber = startNumber;
    }

    /*
     *  Method to increase the number by 1 when calling the next car in line.
     */
    public void incrementNumber()
    {
        this.currentNumber++;
    }

    /*
     *  Method to print the display due to the lack of a display.
     *  @return Returns a string of the current number displayed.
     */
    @Override
    public String toString() {
        return "Now serving: " + currentNumber;
    }
}
