package sem3.integration;

import sem3.model.CreditCard;

/*
 *  Contain the payment authorization object and method.
 *  Always returns true because it's a external system.
 */
public class PaymentAuthorization
{
    /*
     *  Creates a new instance of PaymentAuthorization.
     */
    public PaymentAuthorization()
    {

    }

    /*
     *  Method to verify the payment.
     *  Always returns true due to this being a dummy authentication.
     *
     *  @param cardInfo Receives credit card to authorize.
     *  @param price Receives price to authorize against.
     */
    public boolean authorizePayment (CreditCard cardInfo, double price)
    {
        return true;
    }
}
