package sem3.view;

import sem3.controller.Controller;

import java.util.Scanner;

/*
 * Contains the view declaration and the method for a sample execution of the program.
 */
public class View
{
    private Controller ctrl;

    /*
     *  Creates a new instance of View.
     *
     *  @param ctrl Take a reference to the controller object
     */
    public View (Controller ctrl)
    {
        this.ctrl = ctrl;
        ctrl.addInspectionObserver(new InspectionStatsView());
    }

    /*
     *  Method to execute the program once.
     *  Handles all the calls to the controller
     *  In the loop you also want some kind of input that stops the loops and waits
     *  for the result of the ongoing inspection before fetching the next.
     */
    public void sampleExec()
    {
        Scanner input = new Scanner(System.in);
        double price = 0;
        ctrl.callNextInspection();
        ctrl.operateDoor(false);

        System.out.println("Please enter registration number.");
        String reg = input.next();
        try
        {
            price = ctrl.enterRegNumber(reg);
        }
        catch(Exception noInspectionsFound)
        {
            ctrl.logHandler.logException(noInspectionsFound);
            System.out.println("No inspections found.");
        }

        System.out.println("Amount to pay: " + price);
        ctrl.payment(price);

        for (int i = 0; i < ctrl.inspections.size(); i++)
        {
            ctrl.nextInspection(i, ctrl.inspections);
            ctrl.storeResult(i, "Not good but ok!", true);
        }

        ctrl.operateDoor(true);
        ctrl.operateDoor(false);
    }
}
