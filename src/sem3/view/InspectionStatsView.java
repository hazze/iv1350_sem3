package sem3.view;

import sem3.integration.Inspection;
import sem3.model.InspectionObserver;

class InspectionStatsView implements InspectionObserver
{
    private int noOfPassedInspections, noOfFailedInspections = 0;

    /**
     * Creates a new instance of the inspectionStatsView object.
     */
    InspectionStatsView()
    {

    }

    /**
     * Adds the newly saved inspection and prints the current stats.
     * @param inspectionResult  Receives the newly saved inspection.
     */
    public void newInspection(Inspection inspectionResult)
    {
        addNewInspection(inspectionResult);
        printCurrentState();
    }

    /**
     * Increases the number of passed or failed inspections.
     * @param inspection    Takes the newly saved inspection to check if it's pass or fail.
     */
    private void addNewInspection(Inspection inspection)
    {
        if(inspection.getResult().isInspectResult())
        {
            noOfPassedInspections++;
        }
        else
        {
            noOfFailedInspections++;
        }
    }

    /**
     * Prints the current state. Acts as the "display" that shows the stats.
     */
    private void printCurrentState()
    {
        float percent = ((noOfPassedInspections * 100.0f) / (noOfPassedInspections + noOfFailedInspections));
        System.out.println("Performed inspections: " + (noOfFailedInspections+noOfPassedInspections));
        System.out.println(noOfPassedInspections + " passed.");
        System.out.println(noOfFailedInspections + " failed.");
        System.out.println(percent + "% passed.");
    }
}