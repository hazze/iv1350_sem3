package sem3.startup;

import sem3.controller.Controller;
import sem3.view.*;

import java.io.IOException;

/*
 *  Program main
 */
public class Main {

    /*
     *  Main startup of the program. Creates a instance of the controller and sends it to the newly created view.
     */
    public static void main(String[] args) throws IOException {
        try
        {
            Controller ctrl = new Controller();
            View view = new View(ctrl);
            view.sampleExec();
        }
        catch(IOException exception)
        {
            System.out.println("CAN'T PRINT TO LOG FILE!");
        }
    }
}
