package sem3.controller;

import sem3.integration.*;
import sem3.model.*;
import sem3.util.LogHandler;

import java.io.IOException;
import java.util.ArrayList;

/*
 *  Handles all the controllers tasks.
 */
public class Controller {
    private Display display;
    private GarageDoor garageDoor;
    private DatabaseManager databaseManager;
    private PaymentAuthorization paymentAuthorization;
    private Printer printer;
    public LogHandler logHandler;

    // The list of inspections used to calculate cost and perform inspections.
    public ArrayList<Inspection> inspections = new ArrayList<>();


    private ArrayList<InspectionObserver> inspectionObservers = new ArrayList<>();

    /*
     * Creates a new instance of a controller with instances of the objects it controls.
     * @exception IOException Required for the logHandler to be able to write to the file.
     * If writing to the file fails the exception is thrown.
     */
    public Controller() throws IOException {
        this.display = new Display(0);
        this.garageDoor = new GarageDoor(false);
        this.databaseManager = new DatabaseManager();
        this.paymentAuthorization = new PaymentAuthorization();
        this.printer = new Printer();
        this.logHandler = new LogHandler();
    }

    /*
     *  Method to enter registration number to fetch inspections for the specific car from the database.
     *  Stores the found inspections in a list of inspection objects and returns
     *  the cost of the found inspections by calling for the calculateCost method
     *  in the Inspection class.
     *
     *  @param regNumber The registration number to search for.
     *  @return The cost of all inspections found.
     *  @exception Throws exception to handle the exception thrown on the lower layer. Exception is caught in view.
     */
    public double enterRegNumber(String regNumber) throws Exception {
        System.out.println("\n// Inspector enters registration number for current customer.");
        try
        {
            inspections = databaseManager.findInspectionByRegNumber(regNumber);
            return Inspection.calculateCost(inspections);
        }
        catch(Exception exception)
        {
            throw new Exception("Unable to find inspections.", exception);
        }

    }

    /*
     *  Method to call for the next inspection. Increments the number on the display and opens the door.
     */
    public void callNextInspection() {
        System.out.println("// Inspector calls for next customer by increasing the queue number on the display.");
        display.incrementNumber();
        System.out.println(display.toString());
        garageDoor.operateDoor(true);
        databaseManager.addInspectionObservers(inspectionObservers);
    }

    /*
     *  Method to close or open the door. It takes a boolean as an argument.
     *  true = open, false = closed
     *
     *  @param doorStatus Boolean to tell if door is opened or closed.
     */
    public void operateDoor(Boolean doorStatus) {
        garageDoor.operateDoor(doorStatus);
    }

    /*
     *  Method to fetch the checklist for a inspection.
     *
     *  @param i Index for next inspection to fetch
     *  @param inspections List of inspections
     *  @return Returns checklist for specific inspection.
     */
    public InspectionChecklist nextInspection(int i, ArrayList<Inspection> inspections) {
        System.out.println("\n// Inspector fetches the next inspection to perform.");


        return inspections.get(i).getChecklist();
    }

    /*
     *  Method to store result from inspections in the database (dummy db in this case)
     *  The method also print's the result with the printer.
     *  Also saves the result to the inspections list so they can be printed.
     *
     *  @param i Index for current inspection to store.
     *  @param inspectComment Inspection result comments.
     *  @param inspectResult Boolean for inspection result. True if passed.
     */
    public void storeResult(int i, String inspectComment, boolean inspectResult) {
        System.out.println("// Inspector stores the result of current inspection in the db.");

        inspections.get(i).getResult().setInspectComment(inspectComment);
        inspections.get(i).getResult().setInspectResult(inspectResult);

        int fetchDbFieldIndex = inspections.get(i).getDbFieldIndex();
        databaseManager.storeResult(inspectComment, inspectResult, fetchDbFieldIndex);
        if ((i + 1) == inspections.size()) {
            System.out.println("\n// The result from all inspections is printed by the printer.\n");
            printer.printResult(inspections);
        }
    }

    /*
     *  Method to manage payment, in this task it only handles credit card payments.
     *  The method checks if the payment is authorized and then prints the receipt.
     *  Customer credit card is created inside because it's a dummy card and will be replaced
     *  with a real card received from a card reader if this went live.
     *
     *  @param price Price calculated earlier used to handle the payment.
     */
    public void payment(double price) {
        CreditCard customerCreditCard = new CreditCard(1234, 123, "5555 6666 7777 8888", "John Doe", "19-04");
        if (paymentAuthorization.authorizePayment(customerCreditCard, price)) {
            printer.printReceipt(price);
        } else {
            int none = 0;
            // Print failed receipt or wait for new card, either way it's not specified
            // what should happen in the specification.
        }
    }

    /**
     * Method to add observers to the list of observers.
     * @param obs   Takes an observer to add to the list of observers.
     */
    public void addInspectionObserver(InspectionObserver obs)
    {
        inspectionObservers.add(obs);
    }

}
