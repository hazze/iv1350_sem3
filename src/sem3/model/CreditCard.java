package sem3.model;

/*
 *  Contains the credit card object.
 */
public class CreditCard
{
    private final int pin, cvc;
    private final String number, holder, date;

    /**
     * Creates a new instance of CreditCard.
     * Receives all the card info on creation.
     *
     * @param pin Reference to the card's pin
     * @param cvc Reference to the card's cvc
     * @param number Reference to the card's number
     * @param holder Reference to the card's holder
     * @param date Reference to the card's date
     */
    public CreditCard(int pin, int cvc, String number, String holder, String date)
    {
        this.pin = pin;
        this.number = number;
        this.holder = holder;
        this.date = date;
        this.cvc = cvc;
    }

    /*
     *  Method to fetch pin info.
     *  @return Returns the card's pin
     */
    public int getPin() {
        return pin;
    }

    /*
     *  Method to fetch cvc info.
     *  @return Returns the card's cvc
     */
    public int getCvc() {
        return cvc;
    }

    /*
     *  Method to fetch card number info.
     *  @return Returns the card's number
     */
    public String getNumber() {
        return number;
    }

    /*
     *  Method to fetch card holder info.
     *  @return Returns the card's holder
     */
    public String getHolder() {
        return holder;
    }

    /*
     *  Method to fetch card date info.
     *  @return Returns the card's date
     */
    public String getDate() {
        return date;
    }
}
