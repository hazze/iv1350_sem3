package sem3.model;

/*
 *  Contains the inspection checklist object.
 *  Checklist inspection id and steps.
 */
public class InspectionChecklist
{
    private String inspectionId;
    private String inspectionSteps;

    /*
     *  Creates a new instance of InspectionChecklist.
     *  Adds inspection ID and inspection steps on creation.
     *
     *  @param inspectionId Receives the inspection ID.
     *  @param inspectionSteps Receives the inspection steps.
     */
    public InspectionChecklist(String inspectionId, String inspectionSteps)
    {
        this.inspectionId = inspectionId;
        this.inspectionSteps = inspectionSteps;
    }

    /*
     *  Method to fetch inspection id info.
     *
     *  @return Returns inspection ID.
     */
    public String getInspectionId()
    {
        return inspectionId;
    }

    /*
     *  Method to fetch inspection steps info.
     *  @ returns inspection steps.
     */
    public String getInspectionSteps()
    {
        return inspectionSteps;
    }

    /*
     *  Method to set inspection id.
     *
     *  @param inspectionId Receives inspection id.
     */
    public void setInspectionId(String inspectionId)
    {
        this.inspectionId = inspectionId;
    }

    /*
     *  Method to set inspection steps.
     *  @param inspectionSteps Receives inspection steps.
     */
    public void setInspectionSteps(String inspectionSteps)
    {
        this.inspectionSteps = inspectionSteps;
    }
}
