package sem3.model;

import sem3.integration.Inspection;

/**
 * A listener interface for receiving notifications about
 * saved inspection. The class that is interested in such
 * notifications implements this interface.
 */
public interface InspectionObserver
{
    /**
     * Invoke when inspection has been saved.
     * @param inspection The inspection that was saved.
     */
    void newInspection(Inspection inspection);
}