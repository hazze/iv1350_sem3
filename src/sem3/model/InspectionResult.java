package sem3.model;

/*
 *  Contains inspection result object.
 *  Result contains inspection comments and the result.
 *  Contains toString methods for the object.
 */
public class InspectionResult
{
    private String inspectComment;
    private boolean inspectResult;

    /*
     *  Creates a new instance of InspectionResult.
     *  Takes inspection comment and result on creation.
     *
     *  @param inspectComment Receives a string for comments about the inspection result.
     *  @param inspectResult Receives the inspection result as boolean, true if passed.
     */
    public InspectionResult(String inspectComment, boolean inspectResult)
    {
        this.inspectComment = inspectComment;
        this.inspectResult = inspectResult;
    }

    /*
     *  Method to fetch inspection comments.
     *  @return Return inspection comment
     */
    public String getInspectComment()
    {
        return inspectComment;
    }

    /*
     *  Method to fetch inspection result info.
     *  @return Return inspection result
     */
    public boolean isInspectResult()
    {
        return inspectResult;
    }

    /*
     *  Method to set inspection comments.
     *  @param inspectResult Receives inspection result comments
     */
    public void setInspectComment(String inspectResult)
    {
        this.inspectComment = inspectResult;
    }

    /*
     *  Method to set inspection result.
     *  @param inspectResult Receives inspection result as boolean, true if passed.
     */
    public void setInspectResult(boolean inspectResult)
    {
        this.inspectResult = inspectResult;
    }

    /*
     *  Method to print inspection result in an easy way.
     *  @returns object as string.
     */
    @Override
    public String toString() {
        return "Inspection result: " + inspectResult + ", Inspection comments: " + inspectComment;
    }

    /*
     *  Method to print the boolean of inspection result as text.
     *  @return boolean as string
     */
    public String toStringBoolean()
    {
        return "" + inspectResult;
    }
}
