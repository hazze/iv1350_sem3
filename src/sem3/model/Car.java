package sem3.model;

/*
 *  Contains the car object
 */
public class Car {
    private String regNumber;

    /*
     *  Creates a new instance of Car.
     *  @param Receives the car's registration number.
     */
    public Car(String regNumber)
    {
        this.regNumber = regNumber;
    }

    /*
     *  Method to get registration number info from the car object.
     *  @return Returns the car's registration number.
     */
    public String getRegNumber()
    {
        return regNumber;
    }
}
